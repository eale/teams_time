import parse_csv


def find_median(lst):
    length = 0
    begin = []
    end = []
    for person in lst:
        begin.append(person['join_time'])
        end.append(person['leave_time'])
    begin.sort(reverse = True)
    end.sort()
    length = len(begin)
    while length > int(len(lst)*0.9):
        dif_beg = abs(begin[0]-begin[1])
        dif_end = abs(end[0] - end[1])
        if dif_beg > dif_end:
            begin.pop(0)
        else:
            end.pop(0)
        length = min(len(end), len(begin))

    result = (end[len(end)-1], begin[len(begin)-1])
    print(result)

    return result


def get_something(lst_with_info, median):
    table = {}
    for person in lst_with_info: # zip(res1, k):
        length = min(person['leave_time'], median[0]) - max(person['join_time'], median[1])
        if person['full_name'] in table.keys():
            table[person['full_name']] = table.get(person['full_name']) + length
        else:
            table[person['full_name']] = length
    res2 = list(table.items())
    res2.sort(key=lambda i: i[1])
    for i in res2:
        print(i)


def main():
    lst_with_info = parse_csv.get_list_with_dicts_info(n=4)
    median = find_median(lst_with_info)
    get_something(lst_with_info, median)


if __name__ == '__main__':
    main()
