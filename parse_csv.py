import csv
import datetime
from copy import deepcopy

PERCENT_OF_PARTS = 0.9
PATTERN_TIME_RUSSIA = '%d.%m.% %H:%M:%S'
PATTERN_TIME_ENGLISH = '%m/%d/%Y %I:%M:%S %p'


def get_dict_from_name_row(row):
    form = ('full_name', 'join_time', 'leave_time', 'duration', 'email', 'role')
    result = dict(zip(form, row))
    return result


def get_rows():
    with open('english.csv', encoding='utf-16') as r_file:
        reader = csv.reader(r_file, delimiter="\t")
        for row in reader:
            yield row


def get_list_with_dicts_info(n:float):
    result = []
    rows_iterator = get_rows()
    count = 0
    try:
        for row in get_rows():
            if count >= 7:
                dict_with_info = get_dict_from_name_row(row)
                result.append(dict_with_info)
            count += 1
    except StopIteration:
        pass
    return add_date_time_to_dict(lst_with_info=result)


def add_date_time_to_dict(lst_with_info):
    lst_with_info = deepcopy(lst_with_info)
    for person in lst_with_info:
        person['join_time'] = datetime.datetime.strptime(person['join_time'], '%m/%d/%Y, %I:%M:%S %p')
        person['leave_time'] = datetime.datetime.strptime(person['leave_time'], '%m/%d/%Y, %I:%M:%S %p')

    return lst_with_info


def main():
    res1 = []
    k = []
    with open ('english.csv', 'r', encoding='utf-16') as csvfile:
        spamreader = csv.reader(csvfile, delimiter = '\t')
        flag = True
        for i in spamreader:
            if len(i)>5:
                if flag:
                    flag = False
                    continue
                k.append(i[0])
                begin = datetime.datetime.strptime(i[1], '%m/%d/%Y, %I:%M:%S %p')
                end = datetime.datetime.strptime(i[2], '%m/%d/%Y, %I:%M:%S %p')
                res1.append((begin, end))

        print(res1)
        print(k)
    print(get_list_with_dicts_info(n=PERCENT_OF_PARTS))


if __name__ == '__main__':
    main()
